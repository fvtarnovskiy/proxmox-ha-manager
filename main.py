import os
import sys
import subprocess
import time
import vars

# Class to store information about HA VM configuration
class ha_vm:
   def __init__(self, id, current_status, new_status):
        try:
            self.id = int(id)
        except ValueError:
            raise ValueError(f"[ERROR] Error creating new object! vmid: {id} not integer!")
        
        if self.id < 0:
            raise ValueError(f"[ERROR] vmid: {id} above zero!")

        self.current_status = current_status
        self.new_status = new_status

def main():
    # Read and check variables
    init()
    
    # Backup and load information about current configuration
    run_auxiliary_script()

    # Load info to list
    vm_list = load_vm_info(vars.VM_LIST_PATH, vars.CURRENT_STATUS_PATH)
  
    # Validate vm list
    validate_vm_list(vm_list)

    # Confirm changes
    if confirm_changes(vm_list):
        # If changes confirmed
        apply_changes(vm_list, vars.PAUSE_DURATION)
    else: 
        # Otherwise exit programm
        print('Changes cancelled, nothing to do')

#
# Read variables
# 
def init():

    # Chech that file exists
    def check_path(path):
        if (os.path.exists(path)):
            print(f'{path} exists.. - OK')
        else:
            raise Exception(f'[ERROR] File {path} not found!')

    # Check VM_LIST
    def check_vm_list_var():
        # Chech that file exists
        check_path(vars.VM_LIST_PATH)

        # Chech that file not empty
        if (os.stat(vars.VM_LIST_PATH).st_size != 0):
            print('VM_LIST_PATH not empty..')
        else:
            raise Exception('[ERROR] File from VM_LIST_PATH is empty!')
    
    print("Checking variables..")
    check_vm_list_var()
    check_path(vars.CURRENT_STATUS_PATH)
    check_path(vars.GET_CURRENT_CONFIG_SCRIPT_PATH)
    if int(vars.PAUSE_DURATION)  <= 0:
        raise Exception(f'[ERROR] PAUSE_DURATION cannot be <= 0')

    print()


#
# Load vm list
#
def load_vm_info(vm_to_change_path, current_vm_status):
    vm_to_change_list = []

    # Fill list vm to change
    print("Loading the vm_to_change_list..")
    with open(vm_to_change_path) as file:
        for line in file:
            vm_info = line.strip().split()
            vm_to_change_list.append(ha_vm(id=vm_info[0],
                                current_status=None,
                                new_status=vm_info[1]))

    # Fetch current status
    print("Fetching current status..")
    with open(current_vm_status) as file:
        # Reading every line from current_vm_status
        for line in file:
            vm_info = line.strip().split()

            # Itterating in vm_to_change_list
            for vm in vm_to_change_list:
                # And fill current_status if vm.id is equal
                if vm.id == int(vm_info[0]):
                    print(f'vmid: {vm.id} current_status: {vm.current_status} -> {vm_info[1]}')
                    vm.current_status = vm_info[1]
    
    print("Print current objects:")
    for vm in vm_to_change_list:    
        print(vm.id, vm.current_status, vm.new_status)

    print()
    return vm_to_change_list

#
# Validate vm list
#
def validate_vm_list(vm_list):
    print('Validate vm_list..', end="")

    for vm in vm_list:
        if vm.current_status not in vars.VM_ALLOWED_STATUSES:
            raise ValueError(f"\n[ERROR] vmid: {vm.id} have incorrect current_status {vm.current_status}")
        if vm.new_status not in vars.VM_ALLOWED_STATUSES:
            raise ValueError(f"\n[ERROR] vmid: {vm.id} have incorrect new_status {vm.new_status}")
    print(' - OK')
    print()

#
# Execute auxiliary script
#
def run_auxiliary_script():
        # Add exec for owner
        print('Adding execute permission for owner..')
        subprocess.check_call(f'chmod u+x -v {vars.GET_CURRENT_CONFIG_SCRIPT_PATH}', shell=True)
        
        # Run bash script and give it path to current status as an argument
        print('Starting auxiliary script..')
        command = f'{vars.GET_CURRENT_CONFIG_SCRIPT_PATH} {vars.CURRENT_STATUS_PATH}'
        command_return_code = subprocess.check_call(command, shell=True)
        
        # Check script return code
        if command_return_code != 0:
            raise Exception(f'[ERROR] Script {vars.GET_CURRENT_CONFIG_SCRIPT_PATH} returned non zero exit code!')

#
# Shows changes and asks for confirmation
#
def confirm_changes(vm_list):
    print('Changes need confirmation:')
    for vm in vm_list:
        print(f'vm_id: {vm.id}, status: {vm.current_status} => {vm.new_status}')

    valid = {"yes": True, "no": False}

    # Ask confirmation
    while True:
        sys.stdout.write("Would you like to continue? Please respond with 'yes' or 'no'\n")
        choice=input().lower()

        if choice in valid:
            print()
            return valid[choice]
    

#
# Execute changes
#
def apply_changes(vm_list, pauseDuration):
    print("Applying changes.. ")

    for vm in vm_list:
        # Prepare command
        command = f'ha-manager set vm:{vm.id} --state {vm.new_status}'
        print(f'Execute command \'{command}\'', end='')
        
        # Execute command
        command_return_code = subprocess.check_call(command, shell=True)
        
        if command_return_code == 0:
            print('- OK')

        # Delay between command execution
        time.sleep(pauseDuration)

    print("All commands executed successfully!")

main()