#!/bin/bash

#
# The script saves information about the current HA VM configuration.
#

# Current time
current_time=$(date "+%Y.%m.%d-%H.%M.%S")
# Path to backup directory
backup_dir_path='./backups'
# Path to CURRENT_STATUS_PATH
if [ -z $1 ];
    then 
        echo "[ERROR] CURRENT_STATUS_PATH undefined!" >> /dev/stderr;
        exit 1;
    else 
        CURRENT_STATUS_PATH=$1;
fi

# Save current HA VM status
echo "Saving current HA VM status..."
ha-manager status | grep service | sed 's/:/ /g' | sed 's/)/ /g' | awk '{print $3, $5}' | tee -a $backup_dir_path/current_status.$current_time.txt

# Save current HA VM configuration
echo "Saving HA VM configuration.."
ha-manager config | tee -a $backup_dir_path/current_config.$current_time.txt

# Copy information for the main script
echo "Copying information for the main script"
cp -v $backup_dir_path/current_status.$current_time.txt $CURRENT_STATUS_PATH