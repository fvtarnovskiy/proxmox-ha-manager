import os
dir_path = os.path.dirname(__file__)

VM_LIST_PATH = f'{dir_path}/files/VMs.txt'
CURRENT_STATUS_PATH = f'{dir_path}/files/current_status.txt'
GET_CURRENT_CONFIG_SCRIPT_PATH = f'{dir_path}/get_current_config.sh'
VM_ALLOWED_STATUSES = ['started', 'stopped', 'ignored']
# Seconds
PAUSE_DURATION = 5
